#include "unity.h"
#include "roche_papier_ciseaux.c"

void setUp(void) {}

void tearDown(void) {}

void test_hasard(void) {
    char choix = hasard();
    TEST_ASSERT_TRUE(choix == 'R' || choix == 'P' || choix == 'C');
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_hasard);
    return UNITY_END();
}
