#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Fonction pour générer un choix aléatoire de Roche, Papier ou Ciseaux (R, P, C)
char hasard() {
    srand(time(NULL));
    int random_choice = rand() % 3;
    if (random_choice == 0) {
        return 'R';
    } else if (random_choice == 1) {
        return 'P';
    } else {
        return 'C';
    }
}

// Fonction pour comparer le choix du joueur et de l'ordinateur
void comparaison(char joueur, char ordinateur) {
    printf("Joueur : %c\n", joueur);
    printf("Ordinateur : %c\n");

    if (joueur == ordinateur) {
        printf("Égalité !\n");
    } else if ((joueur == 'R' && ordinateur == 'C') ||
               (joueur == 'P' && ordinateur == 'R') ||
               (joueur == 'C' && ordinateur == 'P')) {
        printf("Le joueur gagne !\n");
    } else {
        printf("L'ordinateur gagne !\n");
    }
}

int main() {
    char joueur;
    printf("Choisissez Roche (R), Papier (P) ou Ciseaux (C) : ");
    scanf(" %c", &joueur);

    char ordinateur = hasard();
    comparaison(joueur, ordinateur);

    return 0;
}
